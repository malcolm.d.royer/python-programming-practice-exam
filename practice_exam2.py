#!usr/bin/env python3

''' Given a string of multiple words separated by single spaces, return
    a new string with the sentence reversed. The words themselves
    should remain as they are. For example, given 'it is accepted as a
    masterpiece on strategy', the returned string should be 'strategy
    on masterpiece a as accepted is it'. '''
def q1(sentence):

    list = []
    newString = ""

    list = sentence.split()

    list.reverse()

    newString = " ".join(list)

    return newString

''' Given a positive integer, return its string representation with
    commas seperating groups of 3 digits. For example, given 65535 the
    returned string should be '65,535'. '''
def q2(n):

    newString = ""

    newString = "{:,}".format(n)

    return newString

''' Given two lists of integers, return a sorted list that contains all
    integers from both lists in descending order. For example, given
    [3,4,9] and [8,1,5] the returned list should be [9,8,5,4,3,1]. The
    returned list may contain duplicates. '''
def q3(lst0, lst1):

    newList = lst0

    newList.extend(lst1)

    newList.sort()

    newList.reverse()

    return newList

''' Given 3 scores in the range [0-100] inclusive, return 'GO' if the
    average score is greater than 50. Otherwise return 'NOGO'. '''
def q4(s1,s2,s3):

    avg = (s1 + s2 + s3) / 3

    if avg > 50:

        return "GO"

    else:

        return "NOGO"

''' Given an integer and limit, return a list of even multiples of the
    integer up to and including the limit. For example, if integer==3
    and limit==30, the returned list should be [0,6,12,18,24,30].
    Note, 0 is a multiple of any integer except 0 itself. '''
def q5(integer, limit):

    list = []
    i = 0

    while i * integer <= limit:

        if i % 2 == 0:

            list.append(i * integer)

        i += 1

    return list

''' Given two filenames, return a list whose elements consist of line
    numbers for which the two files differ. The first line is
    considered line 0. '''
def q6(f0, f1):

    list = []
    lineNum = 0
    lineA = "0"
    lineB = "0"

    with open(f0) as src1, open(f1) as src2:

        while lineA != "":

            lineA = src1.readline()
            lineB = src2.readline()

            if lineA != lineB:

                list.append(lineNum)

            lineNum += 1

    return list

''' Return the first duplicate value in the given list. For example, if
    given [5,7,9,1,3,7,9,5], the returned value should be 7. '''
def q7(lst):

    list = []

    for i in lst:

        if i in list:

            return i

        else:

            list.append(i)

''' Given a sentence as a string with words being separated by a single
    space, return the length of the shortest word. '''
def q8(strng):

    list = []
    shortestLength = 0

    list = strng.split(" ")

    shortestLength = len(list[0])

    for i in list:

        if len(i) < shortestLength:

            shortestLength = len(i)

    return shortestLength

''' Given an alphanumeric string, return the character whose ascii
    value is that of the integer represenation of all of the digits in
    the string concatenated in the order in which they appear. For
    example, given 'hell9oworld7', the returned character should be 'a'
    which has the ascii value of 97. '''
def q9(strng):

    list = []
    int1 = 0

    for i in strng:

        if i.isdigit():

            list.append(i)

    int1 = int("".join(list))

    return chr(int1)

''' Given a list of positive integers sorted in ascending order, return
    the first non-consecutive value. If all values are consecutive,
    return None. For example, given [1,2,3,4,6,7], the returned value
    should be 6. '''
def q10(arr):

    prev = arr[0]

    i = 0

    while True:

        if i > 0 and arr[i]-1 != prev:

            return arr[i]

        prev = arr[i]
        i += 1

    return None
