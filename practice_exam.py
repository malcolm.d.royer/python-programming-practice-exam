#!usr/bin/env python3

''' Given the floatstr, which is a comma separated string of
    floats, return a list with each of the floats in the
    argument as elements in the list. '''
def q1(floatstr):

    # Create an empty list
    list = []

    # Split the string into a list of elements previously
    # separated by commas and save into "list"
    list = floatstr.split(",")

    # Return the list
    return list

''' Given the variable length argument list, return the
    average of all the arguments as a float '''
def q2(*args):

    # Create an empty float to hold a sum of numbers
    numTotal = 0.0
    # Create an empty float to hold the number of args
    totalArgs = 0.0
    # Create an empty float to hold the final average
    average = 0.0

    # For all positional arguments ("args")...
    for i in args:
        # Add arg to sum
        numTotal += i
        # Increase total number of args by +1
        totalArgs += 1

    # Set "average" equal to sum of numbers divided by
    # amount
    average = numTotal / totalArgs

    # Return the average
    return average

''' Given a list (lst) and a number of items (n), return a new
    list containing the last n entries in lst. '''
def q3(lst,n):

    # Create an empty list
    list = []

    # For the items in "lst" starting at "n"...
    for i in range(n,len(lst)):
        # Add item to "list"
        list.append(lst[i])

    # Return the list
    return list

''' Given an input string, return a list containing the ordinal numbers of
    each character in the string in the order found in the input string. '''
def q4(strng):

    # Open the file
    with open(filename) as src:
        # Capture the first line
        firstLine = src.readline()
        # Capture the length of the line
        lineLength = len(firstLine)
    # Create an empty list
    list = []

    # For each character in the string
    for i in strng:
        # Add ordinal value of item to "list"
        list.append(ord(i))

    # Return the list
    return list

''' Given an input string, return a tuple with each element in the tuple
    containing a single word from the input string in order. '''
def q5(strng):

    # Create an empty list
    list = []

    # Set list equal to the words in "strng" split according to whitespace
    list = strng.split()

    # Return the list converted to a tuple
    return tuple(list)

''' Given a dictionary (catalog) whose keys are product names and values are
    product prices per unit and a list of tuples (order) of product names and
    quantities, compute and return the total value of the order. '''
def q6(catalog, order):

    # Create two empty floats
    total = 0.0
    qty_price = 0.0

    # For each product name in the order...
    for i in order:
        # Multiply the quantity by the product's price in the catalog
        qty_price = i[1] * catalog[i[0]]
        # Add to total
        total += qty_price

    # Return the total
    return total

''' Given a filename, open the file and return the length of the first line
    in the file excluding the line terminator. '''
def q7(filename):

    # Create an empty int for the length
    lineLength = 0

    # Open the file
    with open(filename) as src:
        # Capture the first line
        firstLine = src.readline()
        # Capture the length of the line
        lineLength = len(firstLine)

    # Return the length of the line minus the one "\n" at the end
    return lineLength - 1

''' Given a filename and a list, write each entry from the list to the file
    on separate lines until a case-insensitive entry of "stop" is found in
    the list. If "stop" is not found in the list, write the entire list to
    the file on separate lines. '''
def q8(filename,lst):

    # Open the file
    with open(filename,"w") as dst:
        # For each list item...
        for i in lst:
            # If the word is not "stop" (case-insensitive)
            if str(i).lower() != "stop":
                # Write the word to the file on a new line
                dst.write(i+"\n")
            else:
                break

''' Given the military time in the argument miltime, return a string
    containing the greeting of the day.
    0300-1159 "Good Morning"
    1200-1559 "Good Afternoon"
    1600-2059 "Good Evening"
    2100-0259 "Good Night" '''
def q9(miltime):

    # Turn the miltime string into an integer
    time = int(miltime)

    # If time is between 0300-1159
    if time >= 300 and time <= 1159:
        # Return the string "Good Morning"
        return "Good Morning"
    # If the time is between 1200-1559
    elif time >= 1200 and time <= 1559:
        # Return the string "Good Afternoon"
        return "Good Afternoon"
    # If the time is between 1600-2059
    elif time >= 1600 and time <= 2059:
        # Return the string "Good Evening"
        return "Good Evening"
    # If the time is between 2100-0259
    elif time >= 2100 or time <= 259:
        # Return the string "Good Night"
        return "Good Night"

''' Given the argument numlist as a list of numbers, return True if all
    numbers in the list are NOT negative. If any numbers in the list are
    negative, return False. '''
def q10(numList):

    # For each number in numList...
    for i in numList:
        # If it is negative...
        if i < 0:
        # Return "False", otherwise keep going
            return False

    # If nothing broke the "for" loop above (i.e., there were no negative
    # numbers, all were positive), return "True"
    return True
